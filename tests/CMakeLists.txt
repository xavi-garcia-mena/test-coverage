find_package(Boost COMPONENTS unit_test_framework REQUIRED)

set(UNIT_TESTS_SRC test.cpp)

add_executable(unit-tests ${UNIT_TESTS_SRC})

target_link_libraries(unit-tests
					  ${Boost_FILESYSTEM_LIBRARY}
			   		  ${Boost_SYSTEM_LIBRARY}
				      ${Boost_UNIT_TEST_FRAMEWORK_LIBRARY}
                      ${CMAKE_BINARY_DIR}/src/libmy_class_static.a
)
	
add_test(unit-tests 
	     unit-tests
)

