#include "my_class.h"

int MyClass::multiplyByTwo( int n )
{
	return n * 2;

}
int MyClass::multiplyByThree( int n )
{
	return n * 3;
}

int MyClass::multiplyByTwoIfPair( int n )
{
	if ((n % 2) == 0)
	{
		return n * 2;
	}
	else
	{
		return -1;
	}
}

