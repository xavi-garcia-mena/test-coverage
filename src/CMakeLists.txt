# Create a static library so we can use it as a target for the coverage report
add_library(my_class_static STATIC my_class.cpp)

# Add the executable
add_executable(test-coverage main.cpp)
target_link_libraries(test-coverage ${CMAKE_BINARY_DIR}/src/libmy_class_static.a)

