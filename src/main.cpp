#include <iostream>

int main(int /*argc*/, char ** /*argv*/)
{
	std::cout << "How to create a coverage report:" << std::endl;
	std::cout << "mkdir buildcoverage" << std::endl;
	std::cout << "cd buildcoverage" << std::endl;
	std::cout << "cmake -DCMAKE_BUILD_TYPE=coverage .." << std::endl;
	std::cout << "make" << std::endl;
	std::cout << "make test" << std::endl;
	std::cout << "make coverage" << std::endl;

	return 0;
}
